package com.tsc.skuschenko.tm.exception.system;

import com.tsc.skuschenko.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

    public IndexIncorrectException(final String argument) {

        super("Error! Index '" +
                argument + "'. Index should only contain numbers...");
    }

    public IndexIncorrectException(final Integer argument) {
        super("Error! Argument '" + argument + "' must be non-negative...");
    }

}
