package com.tsc.skuschenko.tm.exception.entity.user;

import com.tsc.skuschenko.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login already exist...");
    }

}
