package com.tsc.skuschenko.tm.exception.entity.project;

import com.tsc.skuschenko.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}
