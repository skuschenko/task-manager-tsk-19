package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IAuthService;
import com.tsc.skuschenko.tm.api.service.IUserService;
import com.tsc.skuschenko.tm.exception.empty.EmptyLoginException;
import com.tsc.skuschenko.tm.exception.empty.EmptyPasswordException;
import com.tsc.skuschenko.tm.exception.entity.user.AccessDeniedException;
import com.tsc.skuschenko.tm.model.User;
import com.tsc.skuschenko.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) {
            throw new EmptyPasswordException();
        }
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) {
            throw new AccessDeniedException();
        }
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(
            final String login, final String password, final String email
    ) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

}
