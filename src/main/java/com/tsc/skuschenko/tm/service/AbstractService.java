package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.api.IService;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.entity.EntityNotFoundException;
import com.tsc.skuschenko.tm.model.AbstractEntity;

import java.util.List;

public class AbstractService<E extends AbstractEntity> implements IService<E> {

    private final IRepository<E> entityRepository;

    public AbstractService(IRepository entityRepository) {
        this.entityRepository = entityRepository;
    }

    @Override
    public List findAll() {
        return entityRepository.findAll();
    }

    @Override
    public void add(E entity) {
        if (entity == null) {
            throw new EntityNotFoundException(entity.getClass().toString());
        }
        entityRepository.add(entity);
    }

    @Override
    public E findById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return entityRepository.findById(id);
    }

    @Override
    public void clear() {
        entityRepository.clear();
    }

    @Override
    public E removeById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return entityRepository.removeById(id);
    }

    @Override
    public void remove(E entity) {
        if (entity == null) {
            throw new EntityNotFoundException(entity.getEntityName());
        }
        entityRepository.remove(entity);
    }

}
