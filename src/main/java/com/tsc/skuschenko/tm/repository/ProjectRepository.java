package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project>
        implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        final List<Project> projectsAll = new ArrayList<>(projects);
        projectsAll.sort(comparator);
        return projectsAll;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return projects.size() > index ? projects.get(index) : null;
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeOneByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

}
