package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity>
        implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public List findAll() {
        return entities;
    }

    @Override
    public void add(E entity) {
        entities.add(entity);
    }

    @Override
    public E findById(String id) {
        for (final E entity : entities) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public E removeById(String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public void remove(E entity) {
        entities.remove(entity);
    }

}
