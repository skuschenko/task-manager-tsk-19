package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task>
        implements ITaskRepository {

    private final List<Task> tasks = new ArrayList();

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> tasksAll = new ArrayList<>(tasks);
        tasksAll.sort(comparator);
        return tasksAll;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.size() > index ? tasks.get(index) : null;
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

}
