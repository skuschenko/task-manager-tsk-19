package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskFinishByNameCommand extends AbstractTaskCommand {

    private final String NAME = "task-finish-by-name";

    private final String DESCRIPTION = "finish task by name";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("name");
        final String value = TerminalUtil.nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        final Task task = taskService.completeTaskByName(value);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
