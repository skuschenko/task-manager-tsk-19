package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskChangeStatusByNameCommand extends AbstractTaskCommand {

    private final String NAME = "task-change-status-by-name";

    private final String DESCRIPTION = "change task by name";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("name");
        final String value = TerminalUtil.nextLine();
        final ITaskService taskService = serviceLocator.getTaskService();
        Task task = taskService.findOneByName(value);
        if (task == null) throw new TaskNotFoundException();
        task = taskService.changeTaskStatusByName(
                value, readTaskStatus()
        );
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
