package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;

public class TaskClearCommand extends AbstractTaskCommand {

    private final String NAME = "task-clear";

    private final String DESCRIPTION = "clear all tasks";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        final ITaskService taskService = serviceLocator.getTaskService();
        taskService.clear();
    }

}
