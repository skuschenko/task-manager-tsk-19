package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    private final String NAME = "project-update-by-id";

    private final String DESCRIPTION = "update project by id";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("id");
        final String valueId = TerminalUtil.nextLine();
        final IProjectService projectService
                = serviceLocator.getProjectService();
        Project project = projectService.findById(valueId);
        if (project == null) throw new ProjectNotFoundException();
        showParameterInfo("name");
        final String valueName = TerminalUtil.nextLine();
        showParameterInfo("description");
        final String valueDescription = TerminalUtil.nextLine();
        project = projectService.updateOneById(
                valueId, valueName, valueDescription
        );
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}
