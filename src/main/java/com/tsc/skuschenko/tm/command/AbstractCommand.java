package com.tsc.skuschenko.tm.command;

import com.tsc.skuschenko.tm.api.service.IServiceLocator;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(final IServiceLocator IServiceLocator) {
        this.serviceLocator = IServiceLocator;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute();

    protected void showOperationInfo(final String info) {
        System.out.println("[" + info.toUpperCase() + "]");
    }

    protected void showParameterInfo(final String info) {
        System.out.println("ENTER " + info.toUpperCase() + ":");
    }

}
