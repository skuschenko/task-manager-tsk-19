package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.Collection;

public class HelpShowCommand extends AbstractCommand {

    private final String NAME = "help";

    private final String DESCRIPTION = "help";

    private final String ARGUMENT = "-h";

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        final Collection<AbstractCommand> commands =
                serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            if (command == null) continue;
            String result = "";
            if (command.name() != null && !command.name().isEmpty()) {
                result += command.name();
            }
            if (command.arg() != null && !command.arg().isEmpty()) {
                result += " [" + command.arg() + "]";
            }
            if (command.description() != null
                    && !command.description().isEmpty()) {
                result += " - " + command.description();
            }
            System.out.println(result);
        }
    }

}
