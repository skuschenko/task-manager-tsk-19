package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

public class TaskBindByProjectCommand extends AbstractTaskCommand {

    private final String NAME = "bind-task-by-project";

    private final String DESCRIPTION = "bind task by project";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        showParameterInfo("project id");
        final String projectId = TerminalUtil.nextLine();
        showParameterInfo("task id");
        final String taskId = TerminalUtil.nextLine();
        final IProjectTaskService projectTaskService =
                serviceLocator.getProjectTaskService();
        Task task = projectTaskService.bindTaskByProject(projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}