package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.Collection;

public class AllArgumentsShowCommand extends AbstractCommand {

    private final String NAME = "arguments";

    private final String DESCRIPTION = "arguments";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        final Collection<String> names =
                serviceLocator.getCommandService().getListArgumentName();
        for (final String name : names) {
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
