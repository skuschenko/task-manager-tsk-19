package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;

public class ProgramExitCommand extends AbstractCommand {

    private final String NAME = "exit";

    private final String DESCRIPTION = "exit";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
