package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    AbstractCommand getCommandByArg(String name);

    AbstractCommand getCommandByName(String name);

    Collection<String> getCommandNames();

    Collection<String> getCommandArgs();

    void add(AbstractCommand abstractCommand);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

}