package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

}
