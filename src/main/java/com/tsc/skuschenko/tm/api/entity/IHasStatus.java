package com.tsc.skuschenko.tm.api.entity;

import com.tsc.skuschenko.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
