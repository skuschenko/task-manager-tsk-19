package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.api.IService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project updateOneById(String id, String name, String description);

    Project updateOneByIndex(Integer index, String name, String description);

    Project startProjectById(String id);

    Project startProjectByIndex(Integer index);

    Project startProjectByName(String name);

    Project completeProjectById(String id);

    Project completeProjectByIndex(Integer index);

    Project completeProjectByName(String name);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusByName(String name, Status status);

}
